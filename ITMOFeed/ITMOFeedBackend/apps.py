from django.apps import AppConfig


class ITMOFeedbackendConfig(AppConfig):
    name = 'ITMOFeedBackend'
